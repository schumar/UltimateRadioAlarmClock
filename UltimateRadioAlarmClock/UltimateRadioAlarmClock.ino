#include <TFT_HX8357.h>
#include <TimeLib.h>
#include <Encoder.h>
#include <FastLED.h>

#include "pitches.h"
#include "themes.h"
#include "layout.h"

/* *********************
   *** CONFIG
   ********************* */

bool alarmActive = true;
byte alarmTimeH = 6;
byte alarmTimeM = 0;

byte pinSpeaker = 8;
bool toneActive = true;

const byte maxTimers = 6;

const byte pinRotSwitch = 17;
const byte pinRotA = 19;
const byte pinRotB = 18;

const byte pinDisplayPower = 16;

const byte pinLoad = 13;
const bool invertLoadLED = false;

const byte PixelCount = 6;

// baudrate communication to ESP
#define BAUD 115200

// timeDrift = 1e9/ppm
// e.g. for 0.7% too fast:   timeDrift = 1e9/7000 = 142_857
// time (in seconds) will be adjusted by (- millis()/timeDrift)
long int timeDrift = -151350;
// how quick should we adjust to NTP?
unsigned int adjSpeed = 10;

char const * dayName[7] = {
    "Samstag", "Sonntag",
    "Montag", "Dienstag", "Mittwoch", "Donnerstag", "Freitag"
};

byte selectedTheme = 0;

// Sunrise theme

typedef struct {
    int time;    // seconds
    int r, g, b; // 0..1000
} seq_color_t;

const unsigned int speedup = 4;
#define SUNRISE_SEQUENCE_LEN 10
static const seq_color_t c_sunrise_sequence[SUNRISE_SEQUENCE_LEN] = {
    {-3600,    0,    0,    0},
    {-3000,    0,    0,   20}, // Dark blue
    {-2400,   50,   20,   50},
    {-1800,  100,   20,   50}, // Sun begins to rise
    {-1500,  200,  100,   50},
    {-1200,  500,  300,   50}, // Yellowish
    { -900,  600,  400,  200}, // White
    {    0, 1000,  700,  400}, // Bright white
    { 3300, 1000,  700,  400}, // Begin to fade off
    { 3600,    0,    0,    0}
};

// Ensure that work() is done every workTimeout ms
const unsigned int workTimeout = 10e3;

/* *********************
   *** GLOBALS
   ********************* */

TFT_HX8357 tft = TFT_HX8357();

byte lightMode = 0; // 0 = OFF, 1 = ON, 2 = Fire, 3 = Sunrise

void pingESP();
unsigned long int lastESPpong = 0;

bool forceDisplayUpdate = true;

int melody[] = { NOTE_C4, NOTE_G3, NOTE_G3, NOTE_A3,
    NOTE_G3, 0, NOTE_B3, NOTE_C4 };
int noteDurations[] = { 4, 8, 8, 4,
    4, 4, 4, 4 };
enum {
    TONE_PRESS, TONE_ROT_L, TONE_ROT_R
};

struct timer_t {
    time_t time;
    void (*function)();
};
timer_t timers[maxTimers];

Encoder rotEnc(pinRotA, pinRotB);

bool displayOn = false;     // display is "off" when booting

CRGB leds[PixelCount];

unsigned long int lastNTPUpdate = 0;
unsigned long int lastNTPQuery = 0;

// time (in seconds since 1970, e.g. "Unix Timestamp") when
// the clock was booted
unsigned long int bootTime = 1483225200;    // 2017-01-01 00:00 CET

theme_t theme = availableTheme[selectedTheme];

uint8_t infoBlock[512] = "no info yet.";
void updateWebinfo();

char buf[256];

// last time work() was done; needed for serial timeout
unsigned long int lastWork = 0;

/* *********************
   *** MENU
   ********************* */

int8_t currentMenu = 0;
bool currentlyInMenu = false;

struct menu_t {
    char const * text;
    uint8_t nrItems;
    int8_t selectedItem;
    void (*itemTextFunc)(char * text, uint8_t itemNr);
    void (*changeFunc)();
};

void triggerText(char * text, uint8_t itemNr);
void lightModeText(char * text, uint8_t itemNr);
void onOffText(char * text, uint8_t itemNr);
void hourMenuText(char * text, uint8_t itemNr);
void minuteMenuText(char * text, uint8_t itemNr);
void changeTest();
void changeLight();
void changeLCDOff();
void changeTone();
void changeAlarm();
void changeHour();
void changeMinute();

const uint8_t maxMenus = 7;
menu_t menu[maxMenus] = {
    { "Test",   1,  0, triggerText,    changeTest },
    { "LCDOff", 1,  0, triggerText,    changeLCDOff },
    { "Light",  4,  lightMode,    lightModeText,  changeLight },
    { "Tone",   2,  1, onOffText,      changeTone },
    { "Alarm",  2,  1, onOffText,      changeAlarm },
    { "Hour",   24, alarmTimeH,   hourMenuText,   changeHour },
    { "Minute", 12, alarmTimeM/5, minuteMenuText, changeMinute },
};


/* *********************
   *** SETUP
   ********************* */

void setup() {
    Serial3.begin(BAUD);    // communication with ESP
    Serial.begin(230400);   // debug output via USB

    // Setup the LCD
    pinMode(pinDisplayPower, OUTPUT);
    powerDisplay(true);

    // pull up rotary switch pin
    pinMode(pinRotSwitch, INPUT_PULLUP);

    // "Load" LED
    pinMode(pinLoad, OUTPUT);
    digitalWrite(pinLoad, invertLoadLED ? LOW : HIGH);

    FastLED.addLeds<NEOPIXEL, 54>(leds, PixelCount);
    // as long as sunrise scheme is already color-corrected, don't let FastLED
    // correct a second time. Switch to "TypicalPixelString" when sunrise scheme
    // is NOT color-corrected
    FastLED.setCorrection(UncorrectedColor);
    fill_solid(leds, PixelCount, CRGB::Black);
    FastLED.show();

    showSplashScreen();

    showFrame();

    // seed the RNG
    randomSeed(analogRead(1));

    // wait 1.5s -- the time it takes for the ESP to finish booting and connect
    // to WiFi
    for (int i = 0; i < 1500/20; i++) {
        delay(20);
        handleSerial();
    }

    timerClearall();

    updateNTP();
    // wait for a reply
    delay(200); handleSerial();
    setTimer(nowUnix()+10, pingESP);

    setTimer(nowUnix()+5, updateWebinfo);
}


/* *********************
   *** LOOP
   ********************* */

void loop() {
    myDelay(10000);
}

void work() {
    handleRotary();
    handleSerial();
    updateDisplay();
    updateLEDs();
    timerHandle();

    unsigned long int now = millis();
    lastWork = now;

    // every 50s (or 2s if we never got an reply), try to get an NTP update
    // as the timer is ~0.6% off, that will ensure a maxmimum
    // error of 300ms, well below 1s
    if (
            now - lastNTPQuery > 2e3  // don't send more often then every 2s
            and (
                lastNTPUpdate == 0    // time not set at all
                or now - lastNTPUpdate > 50e3  // standard case: every 50s
                )
       ) {
        updateNTP();
    }

    // After a cold start, if we can't get NTP for too long, trigger an alarm
    // -- maybe there was a power outage which also broke the NTP server, the
    // ESP, the WiFi,...
    if (lastNTPUpdate == 0 and now > 300e3) {
        triggerAlarm();
    }

}

void myDelay(uint16_t ms) {
    unsigned long int start = millis();
    unsigned long int now = start;

    while (now < start + ms) {
        work();
        loadIndicator(false);
        now = millis();
        if ( start + ms - now < 100 ) {
            delay(start + ms - now);
            break;
        } else {
            delay(100);
            loadIndicator(true);
        }
    }
    loadIndicator(true);
}

/* *********************
   *** TIME FUNCTIONS
   ********************* */

unsigned long int nowUnix() {
    // return current Unix timestamp (seconds since 1970)

    static unsigned long int prevMillis = 0;    // for detecting rollover
    unsigned long int nowMillis = millis();

    if (nowMillis < prevMillis) {   // rollover happened!
        // cope by "advancing" bootTime
        bootTime += 4294967;    // (1<<32)/1000
        bootTime -= 4294967295 / timeDrift;
        serialDebugText("URAC: W: millis rollover detected");
    }
    prevMillis = nowMillis;

    unsigned long int ts = bootTime + nowMillis/1000;
    ts -= (long int)(nowMillis>>1) / (timeDrift>>1);
    return ts;
}

void localTime(TimeElements &tm, time_t ts) {
    // convert ts to y,m,d,h,... adjusted by local timezone
    // [XXX] DST handling
    breakTime(ts + (isDST(ts) ? 7200 : 3600), tm);
}

time_t localTS(TimeElements &tm) {
    // convert local y,m,... to timestamp
    // [XXX] DST handling is wrong during switch (as it has to be, as
    // "02:30" happens twice in autumn)
    time_t ts = makeTime(tm);
    return ts - (isDST(ts) ? 7200 : 3600);
}

bool isDST(time_t ts) {
    // [XXX] quick ugly fix for now...
    if (ts <= 1490490000)
        return false;
    else if (ts <= 1509238800)
        return true;
    else
        return false;
}

void handleNTP(unsigned long int secsSince1970) {
    unsigned long int hwTime = nowUnix();
    unsigned long int runTime = millis();

    long int offset = secsSince1970 - hwTime;

    if (offset < -100) {
        sprintf(buf, "NTP: W: ignoring adj of %ld secs", offset);
        serialDebugText(buf);
        return;
    } else if (offset > 100) {
        // Only accept big offsets at startup
        if (lastNTPUpdate == 0) {
            bootTime += offset;
            sprintf(buf, "NTP: I: startup, adj by %ld secs", offset);
            serialDebugText(buf);

            // if the time has changed more than a few hours, ensure
            // that the alarm times are set correctly; this will
            // also forceDisplayUpdate, i.e. it is ensured that the
            // DoW will be updated
            setAlarm();
            setNightlyDisplayOff();
        } else {
            sprintf(buf, "NTP: W: ignoring adj of %ld secs", offset);
            serialDebugText(buf);
            return;
        }
    } else if (offset < -1 or offset > 1) {

        long int error = runTime/1e3 - (secsSince1970 - bootTime);

        if (timeDrift > 0) {
            // positive timeDrift: millis() is running too fast
            // check that this is actually true...
            if (error >= 0) {
                // hwTime = bootTime MINUS millis()/abs(timeDrift)
                timeDrift += adjSpeed * timeDrift * offset / (runTime / 1e3);
            } else {
                timeDrift = -1e6;   // otherwise just set to -0.1%
            }

        } else {
            // negative timeDrift: millis() is running too slow
            // check that this is actually true...
            if (error <= 0) {
                // hwTime = bootTime PLUS millis()/abs(timeDrift)
                timeDrift -= adjSpeed * timeDrift * offset / (runTime / 1e3);
            } else {
                timeDrift = +1e6;   // otherwise just set to +0.1%
            }
        }

        // recalculate current time
        unsigned long int hwTimeNew = nowUnix();
        sprintf(buf, "off %+ld err %+ld adj %+ld=>%+ld td %+ld",
                offset, error, (hwTimeNew-hwTime), -((long int)(runTime>>1))/(timeDrift>>1), timeDrift);
        serialDebugText(buf);
    }
    // else: ignore all smaller changes

    lastNTPUpdate = runTime;
}

/* *********************
   *** TIMERS
   ********************* */

void timerClearall() {
    for (byte i = 0; i < maxTimers; i++) {
        timers[i].time = 0;
        timers[i].function = NULL;
    }
    showTimers();
}

void timerHandle() {
    time_t now = nowUnix();
    for (byte i = 0; i < maxTimers; i++) {
        if (timers[i].function and
                timers[i].time > 0 and
                now >= timers[i].time) {
            void (*function)();
            // sprintf(buf, "TIMER: %d: %ld >= %ld", i, now, timers[i].time);
            // serialDebugText(buf);

            // save function, as this will be deleted before calling it
            function = timers[i].function;
            clearTimer(function);
            function();
        }
    }
    showTimers();
}

bool setTimer(time_t time, void (*function)()) {
    // search for existing timer for given function
    for (byte nr = 0; nr < maxTimers; nr++) {
        if (timers[nr].function == function) {
            timers[nr].time = time;
            showTimers();
            serialDebugText("TIMER: I: Updating timer");
            return true;
        }
    }
    // if none is found, search for a free one
    for (byte nr = 0; nr < maxTimers; nr++) {
        if (timers[nr].function == NULL) {
            timers[nr].time = time;
            timers[nr].function = function;
            showTimers();
            return true;
        }
    }
    serialDebugText("TIMER: E: No free timer found");
    return false;
}

bool clearTimer(void (*function)()) {
    for (byte nr = 0; nr < maxTimers; nr++) {
        if (timers[nr].function == function) {
            timers[nr].time = 0;
            timers[nr].function = NULL;
            showTimers();
            return true;
        }
    }
    serialDebugText("TIMER: W: Timer not found, not deleted");
    return false;
}

void setAlarm() {
    // set Timers for the next Alarm

    if (alarmActive) {
        TimeElements nowTE;
        time_t nowTS = nowUnix();
        breakTime(nowTS, nowTE);

        TimeElements alarmTE = { // S M H Wd D M Y
            0, alarmTimeM, alarmTimeH, 0,
            nowTE.Day, nowTE.Month, nowTE.Year
        };
        time_t alarmTS = localTS(alarmTE);

        // If we are already past the alarm for today, set it for tomorrow (with
        // 5min margin, e.g. also set for tomorrow if it's 06:32 and alarm gets
        // set to 06:35)
        if (alarmTS <= nowTS+300)
            alarmTS += 86400;

        setTimer(alarmTS - (3600/speedup), triggerSunrise);
        setTimer(alarmTS, triggerAlarm);
        setTimer(alarmTS + (3600/speedup) + 10, triggerRestart);

    } else {
        clearTimer(triggerSunrise);
        clearTimer(triggerAlarm);
        clearTimer(triggerRestart);
        // this might happen during "sunRise", so reset the LEDs
        updateLEDs();
    }

    // all timers changed, and the alarm time needs to be redrawn,...
    forceDisplayUpdate = true;
}


/* *********************
   *** ALARM
   ********************* */

void triggerAlarm() {

    wakeDisplay();

    // play melody
    // https://www.arduino.cc/en/Tutorial/ToneMelody
    for (int thisNote = 0; thisNote < 8 and alarmActive; thisNote++) {
        int noteDuration = 1000 / noteDurations[thisNote];
        tone(pinSpeaker, melody[thisNote], noteDuration);
        int pauseBetweenNotes = noteDuration * 1.30;
        tft.invertDisplay(true);
        delay(pauseBetweenNotes/2);
        tft.invertDisplay(false);
        delay(pauseBetweenNotes/2);
        noTone(pinSpeaker);
        work();
    }
}

void triggerSunrise() {
    lightMode = 3;
    forceDisplayUpdate = true;  // need to update the menu
    updateLEDs();
}

void triggerRestart() {
    // ensure that the alarm will trigger tomorrow again :)
    setAlarm();
}


/* *********************
   *** INPUT
   ********************* */

int dividedRotary() {
    // get rotary pos, and divide it by 4
    long int rotPos = rotEnc.read();
    if (rotPos > -2) {
        return (int)((rotPos + 1) / 4);
    } else {
        return (int)((rotPos - 1) / 4);
    }
}

void handleRotary() {
    static bool prevPressed = false;
    bool nowPressed = digitalRead(pinRotSwitch);
    int rotPos = dividedRotary();

    if (rotPos != 0 and displayOn) {
        buttonTone(
                rotPos > 0 ? TONE_ROT_R : TONE_ROT_L,
                rotPos > 0 ? rotPos : -rotPos
                );
        if (currentlyInMenu) {
            // change item
            menu[currentMenu].selectedItem += rotPos;
            while (menu[currentMenu].selectedItem < 0)
                menu[currentMenu].selectedItem += menu[currentMenu].nrItems;
            while (menu[currentMenu].selectedItem >= menu[currentMenu].nrItems)
                menu[currentMenu].selectedItem -= menu[currentMenu].nrItems;
        } else {
            // change menu
            currentMenu += rotPos;
            while (currentMenu < 0)
                currentMenu += maxMenus;
            while (currentMenu >= maxMenus)
                currentMenu -= maxMenus;
        }
        rotEnc.write(0);
    }

    // act on release
    if (prevPressed and not nowPressed) {
        buttonTone(TONE_PRESS, 1);
        if (not displayOn) {
            // if display is off, switch it on
            wakeDisplay();
        } else if (currentlyInMenu) {
            // select item
            menu[currentMenu].changeFunc();
            currentlyInMenu = false;
        } else {
            // enter menu
            currentlyInMenu = true;
        }
    }

    prevPressed = nowPressed;
}

void buttonTone(byte type, byte nr) {
    if (not toneActive or nr == 0) return;
    tone(pinSpeaker, 800 + 500*type, 4);
    for (byte i = 1; i < nr; i++) {
        delay(6);
        tone(pinSpeaker, 800 + 500*type, 4);
    }
}

void triggerText(char * text, uint8_t itemNr) {
    strcpy(text, "Trigger");
}

char * lightModeName[] = {
    "Off", "On", "Fire", "Sunrise"
};
void lightModeText(char * text, uint8_t itemNr) {
    strcpy(text, lightModeName[itemNr]);
}

void onOffText(char * text, uint8_t itemNr) {
    if (itemNr == 0) {
        strcpy(text, "off");
    } else {
        strcpy(text, "on");
    }
}

void hourMenuText(char * text, uint8_t itemNr) {
    sprintf(text, "%02d", itemNr);
}

void minuteMenuText(char * text, uint8_t itemNr) {
    sprintf(text, "%02d", itemNr*5);
}

void changeTest() {
    setTimer(nowUnix() + 5, triggerSunrise);
    //setTimer(nowUnix() + 40, triggerAlarm);
    //setTimer(nowUnix() + 80, triggerRestart);
}

void changeLCDOff() {
    switchOffDisplayDaily();
}

void changeLight() {
    lightMode = menu[currentMenu].selectedItem;
    updateLEDs();
}

void changeTone() {
    toneActive = menu[currentMenu].selectedItem > 0;
}

void changeAlarm() {
    if (menu[currentMenu].selectedItem > 0) {
        if (not alarmActive) {
            // switch on
            alarmActive = true;
            setAlarm();
        }
    } else {
        if (alarmActive) {
            alarmActive = false;
            setAlarm();
        }
    }
}

void changeHour() {
    alarmTimeH = menu[currentMenu].selectedItem;
    setAlarm();
}

void changeMinute() {
    alarmTimeM = menu[currentMenu].selectedItem * 5;
    setAlarm();
}


/* *********************
   *** LEDs
   ********************* */

void updateLEDs() {
    static byte lastUpdateMode = 99;
    static long unsigned int lastUpdateAt = 0;

    // only update dynamic patterns every 20ms
    long unsigned int now = millis();

    if (lightMode == 0) {
        // OFF
        fill_solid(leds, PixelCount, CRGB::Black);

    } else if (lightMode == 1) {
        // ON
        if (lastUpdateMode != lightMode) {
            // we just started; ensure that color correction is set
            FastLED.setCorrection(TypicalPixelString);
        }
        fill_solid(leds, PixelCount, Tungsten40W);

    } else if (lightMode == 2) {
        // Fire
        if (lastUpdateMode != lightMode) {
            // we just started; ensure that color correction is set
            FastLED.setCorrection(TypicalPixelString);
        }
        if (now - lastUpdateAt < 50) return; // don't update too often
        for (unsigned int i=0; i<PixelCount; i++) {
            leds[i] = ColorFromPalette(HeatColors_p, (PixelCount-1-i)*30 + random(30));
        }

    } else if (lightMode == 3) {
        // SunRise
        static int i;  // index of next color
        static int t0; // time when sunrise started

        if (lastUpdateMode != lightMode) {
            // we just started; ensure that color correction is set
            FastLED.setCorrection(UncorrectedColor); // values are corrected
            i = 1;
            t0 = now / 1000;
        }

        if (now - lastUpdateAt < 200) return; // don't update too often

        int r, g, b;
        int t = now / 1000 - t0;
        // sunrise takes quite a while, so we already need to take care of
        // the timeDrift
        t -= t / (timeDrift / 1000);
        t = t * speedup - 3600;

        seq_color_t c1 = c_sunrise_sequence[i - 1];
        seq_color_t c2 = c_sunrise_sequence[i];
        r = (long)(c2.r - c1.r) * (t - c1.time) / (c2.time - c1.time) + c1.r;
        g = (long)(c2.g - c1.g) * (t - c1.time) / (c2.time - c1.time) + c1.g;
        b = (long)(c2.b - c1.b) * (t - c1.time) / (c2.time - c1.time) + c1.b;
        fill_solid(leds, PixelCount, CRGB(r/4, g/4, b/4));

        if (c_sunrise_sequence[i].time < t) {
            i++;
            if (i >= SUNRISE_SEQUENCE_LEN) {    //reached end of sunrise
                lightMode = 0;
                forceDisplayUpdate = true;      // update Light menu
                fill_solid(leds, PixelCount, CRGB::Black);
            }
        }
    }

    FastLED.show();
    lastUpdateMode = lightMode;
    lastUpdateAt = now;
}


/* *********************
   *** SERIAL
   ********************* */

void handleSerial() {

    while (Serial3.available() > 0) {
        char inKey = Serial3.read();
        delay(5);  // wait for more chars

        switch(inKey) {
            case 'Q':   // query
                inKey = Serial3.read();
                switch(inKey) {
                    case 'P':   // ping
                        Serial3.write('A');  // answer
                        Serial3.write('P');  // pong
                        break;
                    default:
                        while (Serial3.available() > 0) {(void)Serial3.read();}
                        serialDebugText("URAC: received unknown Query");
                }
                break;

            case 'A':   // answer
                inKey = Serial3.read();
                switch(inKey) {
                    case 'P':   // pong
                        lastESPpong = millis();
                        break;
                    case 'N':   // NTP
                        unsigned long int hw;
                        unsigned long int lw;
                        uint8_t hb;
                        uint8_t lb;
                        unsigned long int secsSince1970;

                        hb = Serial3.read(); lb = Serial3.read();
                        hw = word(hb, lb);
                        hb = Serial3.read(); lb = Serial3.read();
                        lw = word(hb, lb);
                        secsSince1970 = (hw << 16 | lw) - 2208988800UL;

                        handleNTP(secsSince1970);

                        break;
                    case 'W':   // WebInfo
                        uint16_t len;
                        len = ((uint16_t)Serial3.read() << 8) + Serial3.read();
                        // send "go"
                        Serial3.write('G');

                        for (uint16_t i = 0; i < len and i < 512; i++) {
                            if (waitForSerial()) {
                                infoBlock[i] = Serial3.read();
                            } else {
                                // timeout! take what we got so far and run
                                len = i;
                                break;
                            }
                        }

                        while (Serial3.available() > 0) {(void)Serial3.read();}
                        infoBlock[len] = 0;
                        // debugHexDump(infoBlock, len);
                        showInfoBlock();
                        break;

                    default:
                        while (Serial3.available() > 0) {(void)Serial3.read();}
                        sprintf(buf, "URAC: E: received unknown Answer 0x%02x", inKey);
                        serialDebugText(buf);
                }
                break;

            case 'C':   // command
                inKey = Serial3.read();
                switch(inKey) {
                    case 'D':   // debug print
                        uint8_t len;
                        len = Serial3.read();
                        for (uint8_t i = 0; i < len; i++) {
                            buf[i] = Serial3.read();
                        }
                        buf[len] = 0;
                        serialDebugText(buf);
                        break;
                    default:
                        while (Serial3.available() > 0) {(void)Serial3.read();}
                        sprintf(buf, "URAC: E: received unknown Command 0x%02x", inKey);
                        serialDebugText(buf);
                }
                break;

            default:
                while (Serial3.available() > 0) {(void)Serial3.read();}
                sprintf(buf, "URAC: E: received unknown Message 0x%02x", inKey);
                serialDebugText(buf);
        }
    }
}

bool waitForSerial() {
    // Wait until a byte is available on Serial3, but exit after waiting
    // "too long" (i.e. the global timeout)
    const unsigned int usWait = 100;
    uint16_t expired = millis() - lastWork;

    if (expired > workTimeout) {
        serialDebugText("URAC: E: workTimeout reached");
        return false;
    }

    uint32_t maxwait = ( workTimeout - expired ) * 1000 / usWait;

    for (uint32_t i = 0; i < maxwait; i++) {
        if (Serial3.available()) return true;
        delayMicroseconds(100); // at 115200, 1 Byte =~ 90us
    }

    serialDebugText("URAC: E: workTimeout reached while waiting");
    return false;
}

void serialDebugText(char * text) {
    char buf2[16];  // can't use the global one
    sprintf(buf2, "%10ld ", millis());
    Serial.write(buf2);
    Serial.write(text);
    Serial.write('\n');
}

void debugHexDump(uint8_t * text, uint16_t len) {
    char buf2[16];  // can't use the global one

    for (uint16_t i = 0; i < len; i++) {
        if (i % 16 == 0) {
            sprintf(buf2, "\n0x%04x", i);
            Serial.print(buf2);
        }
        Serial.write(' ');
        sprintf(buf2, "%02hhx", text[i]);
        Serial.write(buf2);
    }
    Serial.write('\n');
}

void updateNTP() {
    Serial3.write('Q');
    Serial3.write('N');
    lastNTPQuery = millis();
    // serialDebugText("NTP: I: sent request to ESP");
}

void pingESP() {
    Serial3.write('Q');
    Serial3.write('P');
    setTimer(nowUnix()+30, pingESP);
}

void updateWebinfo() {
    Serial3.write('Q');
    Serial3.write('W');
    setTimer(nowUnix() / 600 * 600 + 600 + 540, updateWebinfo);
}

/* *********************
   *** DISPLAY
   ********************* */

void updateDisplay() {
    showDateTime();
    showAlarmTime();
    showMenu();
    forceDisplayUpdate = false;
}

void powerDisplay(bool newState) {
    if (newState == displayOn) return;  // nothing changed

    digitalWrite(pinDisplayPower, ! newState); // driving P-MOSFET -> invert

    if (not displayOn and newState) {   // switched on
        tft.init();
        tft.setRotation(2);
        // user might have rotated buttons while the display was off;
        // reset to zero
        rotEnc.write(0);
    } // else? nothing to do (yet) when switchig off

    displayOn = newState;
}

void wakeDisplay() {
    powerDisplay(true);
    showFrame();
    showInfoBlock();
    forceDisplayUpdate = true;
    updateDisplay();
}

void switchOffDisplayDaily() {
    powerDisplay(false);
    setTimer((nowUnix() / 300 * 300 ) + 86400, switchOffDisplayDaily);
}

void setNightlyDisplayOff() {
    // This will be called when a big adjustment is made to the clock via
    // NTP (i.e. at boot). Set display to switch off at 22:00 (local)

    TimeElements nowTE;
    time_t nowTS = nowUnix();
    localTime(nowTE, nowTS);

    TimeElements alarmTE = { // S M H Wd D M Y
        0, 0, 22, 0,
        nowTE.Day, nowTE.Month, nowTE.Year
    };
    time_t alarmTS = localTS(alarmTE);

    // If we are already past 22:00, set it for tomorrow
    if (alarmTS <= nowTS+300)
        alarmTS += 86400;

    setTimer(alarmTS, switchOffDisplayDaily);
}

void setTextColor(fgbg_t fgbg) {
    // wrapper for tft.setTextColor, accepting our fgbg struct as argument
    tft.setTextColor(fgbg.fg, fgbg.bg);
}

void showFrame() {
    tft.fillScreen(theme.frame.bg);
    tft.drawRoundRect(0, 0, tft.width(), tft.height(), 6, theme.frame.fg);
    tft.fillRect(1, tft.height()/3 - (48 + 2),
            tft.width()-2, (48 + 2), theme.date.bg);
    tft.fillRect(1, tft.height()/3,
            tft.width()-2, (48 + 2), theme.clock.bg);
    tft.fillRect(1, tft.height()/3 + (48 + 2),
            tft.width()-2, (24 + 4), theme.date.bg);
}

void showDateTime() {
    static byte lastUpdateSec = 99;
    static byte lastUpdateDay = 99;

    TimeElements tm;
    localTime(tm, nowUnix());

    if (tm.Second == lastUpdateSec and not forceDisplayUpdate) return;

    setTextColor(theme.clock);
    tft.setTextSize(1);

    sprintf(buf, "%02d:%02d:%02d", tm.Hour, tm.Minute, tm.Second);
    tft.drawCentreString(buf, tft.width()/2, tft.height()/3+1, 7);
    lastUpdateSec = tm.Second;

    // NTP sync status
    unsigned long int NTPage = (millis() - lastNTPUpdate) / 1000;
    if (lastNTPUpdate == 0 or NTPage > 3600) {
        tft.fillRect(tft.width() - 20, tft.height()/3 + 20, 8, 8, TFT_RED);
    } else if (NTPage < 55) {
        tft.fillRect(tft.width() - 20, tft.height()/3 + 20, 8, 8, TFT_GREEN);
    } else {
        tft.fillRect(tft.width() - 20, tft.height()/3 + 20, 8, 8, TFT_YELLOW);
    }

    // ping status
    unsigned long int pingAge = (millis() - lastESPpong) / 1000;
    if (lastESPpong == 0 or (pingAge > 40 and pingAge < 100)) {
        tft.fillRect(20, tft.height()/3 + 20, 8, 8, TFT_YELLOW);
    } else if (pingAge <= 40) {
        tft.fillRect(20, tft.height()/3 + 20, 8, 8, TFT_GREEN);
    } else {
        tft.fillRect(20, tft.height()/3 + 20, 8, 8, TFT_RED);
    }

    if (tm.Day == lastUpdateDay and not forceDisplayUpdate) return;

    setTextColor(theme.date);

    sprintf(buf, "%04d-%02d-%02d", tm.Year+1970, tm.Month, tm.Day);
    tft.drawCentreString(buf, tft.width()/2, tft.height()/3 - 48, 6);
    lastUpdateDay = tm.Day;

    // now print the DoW; this will not be done if the DoM has not changed,
    // which is wrong if NTP sets the time 1 month ahead (e.g. from 2017-01-01
    // to 2017-02-01). For this reason, NTP updates will forceDisplayUpdate
    // when such a big change is made

    // but first, clear area, as the names are not all the exact same width
    tft.fillRect(1, tft.height()/3+50, tft.width()-2, 24+2, theme.date.bg);
    tft.drawCentreString(dayName[tm.Wday % 7], tft.width()/2, tft.height()/3+51, 4);

}

void showAlarmTime() {
    static int lastUpdate = 9999;
    fgbg_t color;

    if ((alarmTimeH*60+alarmTimeM) == lastUpdate and not forceDisplayUpdate) return;

    color = alarmActive ? theme.alarmEn : theme.alarmDis;

    tft.fillRect(1, tft.height()/2 - 2, tft.width()-2, 48 + 2, color.bg);

    tft.setTextSize(1);
    setTextColor(color);

    sprintf(buf, "%02d:%02d", alarmTimeH, alarmTimeM);
    tft.drawCentreString(buf, tft.width()/2, tft.height()/2, 7);

    lastUpdate = alarmTimeH * 60 + alarmTimeM;
}

void showInfoBlock() {
    block_t b = layout[INFOBLOCK];

    int16_t font = 2;   // TFT_HX8357 uses "int16_t"
    uint8_t fh = tft.fontHeight(font);
    uint8_t fw = tft.textWidth("x", font);

    setTextColor(theme.infoBlock);
    tft.setTextSize(1);

    tft.fillRect(b.x, b.y, b.w, b.h, theme.infoBlock.bg);

    uint16_t x = 0;
    uint16_t y = 0;
    uint16_t xStored = 0;
    uint16_t yStored = 0;

    for (uint16_t i = 0; infoBlock[i] != 0 and i<512 and y < b.h; i++) {
        uint8_t c = infoBlock[i];

        if (c >= 0x20 and c < 0x80) {   // "normal" character
            if (x < b.w)
                x += tft.drawChar(c, b.x + x, b.y + y, font);

        } else if (c == '\n') {         // new line
            x = 0; y += fh;

        } else if (c == '\t') {         // tab
            x = ((x / fw + 1) / 8 + 1) * 8 * fw;

        } else if (c == 0x84) {         // store horizontal position
            xStored = x;

        } else if (c == 0x85) {         // store vertical position
            yStored = y;

        } else if (c == 0x86) {         // horiz move relative to stored pos
            i++;
            int16_t r = infoBlock[i++] << 8;
            r |= infoBlock[i];
            x = xStored + r;

        } else if (c == 0x87) {         // vert move relative to stored pos
            i++;
            int16_t r = infoBlock[i++] << 8;
            r |= infoBlock[i];
            y = yStored + r;

        } else {                        // unsupported
            if (x < b.w)
                x += tft.drawChar('?', b.x + x, b.y + y, font);
        }

    }
}

void showTimers() {
    tft.setTextFont(1);
    setTextColor(theme.dbgTimers);
    tft.setTextSize(1);

    for (byte i = 0; i < maxTimers; i++) {

        if (timers[i].time > 0) {
            TimeElements tm;
            localTime(tm, timers[i].time);
            sprintf(buf, "%04d-%02d-%02d %02d:%02d:%02d ",
                    1970+tm.Year, tm.Month, tm.Day, tm.Hour, tm.Minute, tm.Second);
        } else {
            sprintf(buf, "%19s ", "OFF");
        }
        tft.setCursor(tft.width()-170, (tft.height()/2+48+10) + (i+1)*8);
        tft.print(buf);

        if (timers[i].function) {
            sprintf(buf, "%p", timers[i].function);
            tft.print(buf);
        } else {
            tft.print("NULL  ");
        }
    }
}

void showMenu() {
    static uint8_t lastUpdateMenu = 255;
    static uint8_t lastUpdateItem = 255;
    static bool lastInMenu = false;

    if (currentMenu == lastUpdateMenu and
            currentlyInMenu == lastInMenu and
            menu[currentMenu].selectedItem == lastUpdateItem and
            not forceDisplayUpdate) return;
    lastUpdateMenu = currentMenu;
    lastInMenu = currentlyInMenu;
    lastUpdateItem = menu[currentMenu].selectedItem;

    tft.setTextFont(2);
    tft.setTextSize(1);

    for (uint8_t i = 0; i < maxMenus; i++) {

        setTextColor(
                i == currentMenu ? (
                        currentlyInMenu ?
                        theme.menuInactSelected :
                        theme.menuActiveSelected
                    ) : (
                        currentlyInMenu ?
                        theme.menuInact :
                        theme.menuActive
                    )
                );

        tft.setCursor(6, tft.height() + (-maxMenus +i)*18 - 20);
        tft.print(menu[i].text);

        tft.setCursor(50, tft.height() + (-maxMenus +i)*18 - 20);

        // show at most 7 items at once
        int8_t startItem = 0; // signed, needed later for calc
        uint8_t maxItems = 7;

        if (menu[i].nrItems <= 7) {
            // simple case, just show all
            maxItems = menu[i].nrItems;
        } else {
            startItem = menu[i].selectedItem - 3;
            if (startItem < 0) {
                startItem = 0;
            } else if (startItem + 6 >= menu[i].nrItems) {
                startItem = menu[i].nrItems - 7;
            }
        }

        for (uint8_t j = 0; j < maxItems; j++) {
            setTextColor(
                    j+startItem == menu[i].selectedItem ? (
                            currentlyInMenu and i == currentMenu ?
                            theme.menuActiveSelected :
                            theme.menuInactSelected
                        ) : (
                            currentlyInMenu and i == currentMenu ?
                            theme.menuActive :
                            theme.menuInact
                        )
                    );

            menu[i].itemTextFunc(buf, j+startItem);
            tft.print(" ");
            tft.print(buf);
        }
    }
}

void showSplashScreen() {

    tft.fillScreen(TFT_ORANGE);
    tft.setTextFont(4);
    tft.setTextSize(2);
    tft.setTextColor(TFT_BLUE, TFT_ORANGE);

    tft.setCursor(20, 20);
    tft.print("Ultimate");
    delay(50);
    tft.setCursor(20, 80);
    tft.print("Radio");
    delay(50);
    tft.setCursor(20, 140);
    tft.print("Alarm");
    delay(50);
    tft.setCursor(20, 200);
    tft.print("Clock");
    delay(50);
    tft.setTextSize(1);
    tft.setCursor(20, 280);
    tft.print(__DATE__);
    tft.print(" ");
    tft.print(__TIME__);

    // delay a bit, to allow the attached ESP to boot
    delay(1000);
}

void loadIndicator(bool load) {
    if (load and displayOn) {
        digitalWrite(pinLoad, not invertLoadLED);
    } else {
        digitalWrite(pinLoad, invertLoadLED);
    }
}

// vim: textwidth=80 shiftwidth=4 expandtab
