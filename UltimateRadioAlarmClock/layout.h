
struct block_t {
    const uint16_t x, y;
    const uint16_t w, h;
};

enum {
    INFOBLOCK,
};

block_t layout[] = {
    {4, 4, 320-2*4, 100},
};
