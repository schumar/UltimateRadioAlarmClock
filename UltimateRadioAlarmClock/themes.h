struct fgbg_t {
    // colors are 16bit (5/6/5) numbers
    uint16_t fg, bg;
};
struct theme_t {
    fgbg_t frame,
           menuActive, menuActiveSelected,
           menuInact, menuInactSelected,
           clock, date, alarmEn, alarmDis,
           infoBlock,
           debug, dbgTimers;
};

const byte nrThemes = 1;
theme_t availableTheme[nrThemes] = {
    { // Theme 0 -- classical
        {TFT_WHITE,     TFT_BLACK},     // frame
        {TFT_LIGHTGREY, TFT_NAVY},      // menuActive
        {TFT_YELLOW,    TFT_BLACK},     // menuActiveSelected
        {TFT_LIGHTGREY, TFT_BLACK},     // menuInactive
        {TFT_YELLOW,    TFT_BLACK},     // menuInactiveSelected
        {TFT_YELLOW,    TFT_DARKGREY},  // clock
        {TFT_YELLOW,    TFT_DARKGREY},  // date
        {TFT_BLUE,      TFT_LIGHTGREY}, // alarm enabled
        {TFT_BLUE,      TFT_BLACK},     // alarm disabled
        {TFT_WHITE,     0xB},           // info block
        {TFT_YELLOW,    TFT_MAROON},    // debug
        {TFT_GREEN,     TFT_BLACK}      // timer list
    }
};

// vim: textwidth=80 shiftwidth=4 expandtab
