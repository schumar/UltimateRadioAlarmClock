#include <ESP8266WiFi.h>
#include <ESP8266mDNS.h>
#include <WiFiClient.h>
#include <WiFiUdp.h>
#include <ArduinoOTA.h>
#include <DNSServer.h>            //Local DNS Server used for redirecting all requests to the configuration portal
#include <ESP8266WebServer.h>     //Local WebServer used to serve the configuration portal
#include <WiFiManager.h>          //https://github.com/tzapu/WiFiManager WiFi Configuration Magic
#include <ArduinoHttpClient.h>    // for retrieving WebInfo

#include <Wire.h>
#include <radio.h>
#include <RDA5807M.h>
#define FIX_BAND     RADIO_BAND_FM   ///< The band that will be tuned by this sketch is FM.
#define FIX_STATION  10290           ///< The station that will be tuned by this sketch is 89.30 MHz.
#define FIX_VOLUME   8               ///< The volume that will be set by this sketch is level 8.

RDA5807M radio;    // Create an instance of Class for RDA5807M Chip


// baudrate communication to URAC
#define BAUD 115200

unsigned int localPort = 2390;      // local port to listen for UDP packets
IPAddress timeServerIP(10, 1, 0, 9);

const int NTP_PACKET_SIZE = 48; // NTP time stamp is in the first 48 bytes of the message
byte packetBuffer[ NTP_PACKET_SIZE]; //buffer to hold incoming and outgoing packets
WiFiUDP udp;

char webinfoServer[] = "10.1.0.9";

WiFiClient wifi;
HttpClient httpClient = HttpClient(wifi, webinfoServer, 80);

String response;
int statusCode = 0;

void setup() {
    WiFiManager wifiManager;

    Serial.begin(BAUD);
    Serial.swap();  // use GPIO 13 and 15 for RX and TX

    wifiManager.autoConnect("URAC"); // this will block

    // Set up OTA
    ArduinoOTA.onStart([]() {
            sendDebugText("I: OTA Start");
    });
    ArduinoOTA.onEnd([]() {
            sendDebugText("I: OTA Done");
    });
    ArduinoOTA.onError([](ota_error_t error) {
            sendDebugText("E: OTA Error");
    });

    ArduinoOTA.setHostname("urac");
    ArduinoOTA.begin();

    sendDebugText("I: Good Morning!");

    // socket to listen for NTP
    udp.begin(localPort);

    // send an NTP query right now -- while the URAC hasn't asked for it,
    // it will definitely do it sooner or later, so let's speed things up
    // a bit :)
    sendNTPpacket(timeServerIP); // send an NTP packet to a time server

    // // Initialize the Radio
    // radio.init();
    // // Set all radio setting to the fixed values.
    // radio.setBandFrequency(FIX_BAND, FIX_STATION);
    // radio.setVolume(0x0);
    // radio.setMono(false);
    // radio.setMute(false);
}

void loop() {
    char buf[64];

    if (!WiFi.isConnected()) WiFi.begin();

    while (Serial.available() > 0) {
        char inKey = Serial.read();

        switch(inKey) {
            case 'Q':
                for (int i = 0; i < 100 and Serial.available() == 0; i++) {
                    delay(1);
                }
                inKey = Serial.read();
                switch(inKey) {
                    case 'P':   // ping
                        Serial.write('A');  // answer
                        Serial.write('P');  // pong
                        break;

                    case 'W':   // WebInfo
                        httpClient.get("/webinfo/");
                        statusCode = httpClient.responseStatusCode();
                        if (statusCode >= 200 and statusCode < 300) {
                            response = httpClient.responseBody();
                            unsigned int len = response.length();

                            Serial.write('A');      // answer
                            Serial.write('W');      // webinfo
                            Serial.write((uint8_t)(len >> 8));
                            Serial.write((uint8_t)(len & 0xff));

                            // wait until Mega is ready. We can't just send the data, as
                            // the RX buffer is only 64 Byte!
                            while (Serial.available() == 0) {delay(1);}
                            inKey = Serial.read();
                            // this is hopefully a "G", for "go", but if it
                            // isn't, we'll still send the data

                            for (uint16_t i = 0; i < len && i < 512; i++) {
                                Serial.write((char)response[i]);

                                // every 16 bytes, wait for a bit, so the
                                // receiver can catch up
                                if ((i % 16) == 15) delay(1);
                            }

                        } else {
                            sprintf(buf, "E: HTTP status %d", statusCode);
                            sendDebugText(buf);
                        }
                        break;

                    case 'N':   // NTP
                        // ensure that no old, unprocessed reply packets are in the queue
                        while (udp.parsePacket());

                        sendNTPpacket(timeServerIP); // send an NTP packet to a time server
                        break;

                    default:
                        while (Serial.available() > 0) {(void)Serial.read();}
                        sprintf(buf, "E: received unknown Query 0x%02x", inKey);
                        sendDebugText(buf);
                }
                break;

            case 0xfe:
                // seems to be some spurious noise at boot, just ignore it
                break;

            default:
                // while (Serial.available() > 0) {(void)Serial.read();}
                sprintf(buf, "E: received unknown Message 0x%02x", inKey);
                sendDebugText(buf);
        }
    }

    // handle NTP reply
    int cb = udp.parsePacket();
    if (cb) {
        udp.read(packetBuffer, NTP_PACKET_SIZE); // read the packet into the buffer
        Serial.write('A');  // answer
        Serial.write('N');  // NTP
        Serial.write(packetBuffer[40]);
        Serial.write(packetBuffer[41]);
        Serial.write(packetBuffer[42]);
        Serial.write(packetBuffer[43]);
    }

    ArduinoOTA.handle();

    delay(10);
}

void sendDebugText(char * text) {
    Serial.write('C');      // command
    Serial.write('D');      // print debug text
    Serial.write((uint8_t)strlen(text) + 5);
    Serial.print("ESP: ");
    Serial.print(text);
}

// send an NTP request to the time server at the given address
void sendNTPpacket(IPAddress& address) {

    // set all bytes in the buffer to 0
    memset(packetBuffer, 0, NTP_PACKET_SIZE);
    // Initialize values needed to form NTP request
    // (see URL above for details on the packets)
    packetBuffer[0] = 0b11100011;   // LI, Version, Mode
    packetBuffer[1] = 0;     // Stratum, or type of clock
    packetBuffer[2] = 6;     // Polling Interval
    packetBuffer[3] = 0xEC;  // Peer Clock Precision
    // 8 bytes of zero for Root Delay & Root Dispersion
    packetBuffer[12]  = 49;
    packetBuffer[13]  = 0x4E;
    packetBuffer[14]  = 49;
    packetBuffer[15]  = 52;

    // all NTP fields have been given values, now
    // you can send a packet requesting a timestamp:
    udp.beginPacket(address, 123); //NTP requests are to port 123
    udp.write(packetBuffer, NTP_PACKET_SIZE);
    udp.endPacket();
}

// vim: textwidth=80 shiftwidth=4 expandtab
