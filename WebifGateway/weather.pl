#!/usr/bin/perl -w
#
use strict;
use warnings;

use LWP::Simple;
use JSON qw(decode_json);
use POSIX qw(strftime);
use CGI;

use Data::Dumper;
$Data::Dumper::Sortkeys = 1;

# see http://bulk.openweathermap.org/sample/city.list.json.gz
my $location = 'id=7871497';

my %desc = (
    '200' => 'Gewitter, leichter Regen',
    '201' => 'Gewitter, Regen',
    '202' => 'Gewitter, starker Regen',
    '210' => 'Leichtes Gewitter',
    '211' => 'Gewitter',
    '212' => 'Schweres Gewitter',
    '301' => 'Nieseln',
    '311' => 'Nieselregen',
    '500' => 'Leichter Regen',
    '501' => 'Regen',
    '502' => 'Schwerer Regen',
    '503' => 'Sehr schwerer Regen',
    '504' => 'Extremer Regen',
    '600' => 'Leichter Schneefall',
    '601' => 'Schneefall',
    '602' => 'Schwerer Schneefall',
    '611' => 'Schneeregen',
    '701' => 'Dunst',
    '741' => 'Nebel',
    '800' => 'Klar',
    '801' => 'Vereinzelt Wolken',
    '802' => 'Leicht bewoelkt',
    '803' => 'Bewoelkt',
    '804' => 'Bedeckt',
);

my $api_key;
if (open my $fh, '<', 'apikey.txt') {
    $api_key = <$fh>;
    chomp $api_key;
} else {
    die "Can't open apikey.txt. $!.";
}

my $q = CGI->new;
print $q->header(-type=>'text/plain', -charset=>'utf-8');

my $url = "http://api.openweathermap.org/data/2.5/forecast?$location&appid=$api_key";

my $weather = decode_json(get($url));
# print Dumper($weather);

my $loc = $$weather{'city'}{'name'};

my @fc;
for my $i (0..$#{$$weather{'list'}}) {
    my $w = $$weather{'list'}[$i];

    $fc[$i]{'time'} = strftime('%Hh', localtime($$w{'dt'}));

    # if (abs( $$w{'main'}{'temp_min'} - $$w{'main'}{'temp_max'}) > 0.3) {
    #     $fc[$i]{'temp'} = sprintf "%4.1f - %4.1f",
    #         celsius($$w{'main'}{'temp_min'}),
    #         celsius($$w{'main'}{'temp_max'});
    # } else {
        $fc[$i]{'temp'} = sprintf "%5.1f",  # 1 char for "-" sign
            celsius($$w{'main'}{'temp'});
    # }

    my $cond = '';
    for my $ww (@{$$w{'weather'}}) {
        $cond .= '; ' unless $cond eq '';
        $cond .= exists $desc{$$ww{'id'}} ?
        $desc{$$ww{'id'}} :
        sprintf "%d:%s/%s", $$ww{'id'}, $$ww{'main'}, $$ww{'description'};
    }
    $fc[$i]{'cond'} = substr($cond, 0, 20);

    $fc[$i]{'wind'} = sprintf "%4.1f", kmh($$w{'wind'}{'speed'});

    my $mm = 0;
    $mm += $$w{'rain'}{'3h'} if exists $$w{'rain'}{'3h'};
    $mm += $$w{'snow'}{'3h'} if exists $$w{'snow'}{'3h'};
    $fc[$i]{'mm'} = sprintf "%4.1fmm", $mm if $mm > 0;

    $fc[$i]{'cloud'} = sprintf "%02d%%", $$w{'clouds'}{'all'};
}

printf "Aktualisiert um %s\n", strftime("%H:%M:%S" ,localtime());

for my $i (0..4) {
    printf "%s ", $fc[$i]{'time'};
    printf "%c", 0x84;               # store horizontal position
    print $fc[$i]{'cond'};
    if (exists $fc[$i]{'mm'}) {
        printf "%c%c%c", 0x86, 0, 90;  # horiz move relative to stored pos
        printf " %s", $fc[$i]{'mm'};
    }
    printf "%c%c%c", 0x86, 0, 138;  # horiz move relative to stored pos

    printf " %s %sC %skm/h\n",
        $fc[$i]{'cloud'}, $fc[$i]{'temp'}, $fc[$i]{'wind'};
}


sub celsius {
    my $K = shift;
    return $K - 273.15;
}

sub kmh {
    my $ms = shift;
    return $ms * 3.6;
}

