# Used Pins

## Arduino Mega

     8 OC4C Speaker
    
    13 LED  Load LED (onboard)
    
    14 TXD3 Serial to ESP
    15 RXD3 Serial from ESP
    
    16      Display power switch
    
    17      Rotary Encoder 1 Switch
    18 INT3 Rotary Encoder 1 Contact B
    19 INT2 Rotary Encoder 1 Contact A
    
    54      LED strip
